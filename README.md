# agentk-prereq

The overall order of execution is:
1. PreRequisites - [agentk-prereq](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-prereq)
2. Create Service Account - [agentk-service-account](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-service-account)
3. Create Network - [agentk-network](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-network)
4. Create Kubernetes Cluster - [agentk-cluster](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-cluster)
5. Configure Cluster Role Bindings - [agentk-cluster-role-binding](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-cluster-role-binding)
6. Create Agent Registration Project - [agentk-registration](https://gitlab.com/sandlin/agentk-registration)
7. Install GitLab Agent - [agentk-agent](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-agent)
9. Configure Agent Registration Project
   1. Modify the [config.yml](https://gitlab.com/sandlin/agentk-registration/-/blob/groot/.gitlab/agents/gitlab-agent/config.yaml)
   1. Set `gitops.manifest_projects.id` to point to your `Cluster Services Management Project`
   2. Set `ci_access.groups.id` to your org.
8. Create Services Management Project - [agentk-services](https://gitlab.com/sandlin/agentk-services)
   1. - This was created by following [Manage Cluster Applications](https://docs.gitlab.com/ee/user/clusters/management_project_template.html#create-a-project-based-on-the-cluster-management-project-template)
   2. Modify the [.gitlab-ci.yml](https://gitlab.com/sandlin/agentk-services/-/blob/groot/.gitlab-ci.yml) to set your `KUBE_CONTEXT`
      1. Set `variables.KUBE_CONTEXT` to point to the registered agent.
      1. Modify the `helmfile.yml` and uncomment what you'd like installed. Example: [helmfile.yml](https://gitlab.com/sandlin/agentk-services/-/blob/groot/helmfile.yaml)
   3. Commit and push the changes. This will trigger a Pipeline execution and install the INGRESS to your cluster.
9. Get the external IP of your INGRESS. Save this value for our project variables.
       ```
       # Make sure you are in the gitlab-managed-apps namespace
       kubens gitlab-managed-apps
       # Get the IP of the ingress.
       kubectl get services -o jsonpath='{..loadBalancer.ingress..ip}' ingress-nginx-ingress-controller
       35.247.53.168
       ```
10. If using a DNS record, you can configure it like:
    ```
     $ gcloud dns record-sets list --zone=papanca
      NAME            TYPE  TTL    DATA
      ...
      *.papanca.com.  A     300    35.247.53.168
    ```

    If you have an A-Record already, the way to reset it is:
    ```
    gcloud dns record-sets delete "*.papanca.com." --type=A --zone=papanca; gcloud dns record-sets create "*.papanca.com." --type=A --zone=papanca --rrdatas=`kubectl get services -o jsonpath='{..loadBalancer.ingress..ip}' ingress-nginx-ingress-controller`
    ```

10. Setup your project's .gitlab-ci.yml file to use the cluster. [example](https://gitlab.com/sandlin/examples/python_flask/-/blob/groot/.gitlab-ci.yml#L8-11)
    - If you are using `nip.io` you can just set your `KUBE_INGRESS_BASE_DOMAIN` in your `.gitlab.ci.yml` to the IP above accordingly. EX: `KUBE_INGRESS_BASE_DOMAIN: '35.247.53.168.nip.io'`
    
## Pre-Requisites

### Google Cloud

##### Required Software
- [Google Cloud SDK (gcloud CLI)](https://cloud.google.com/sdk/docs/install)
- [Terraform CLI](https://learn.hashicorp.com/tutorials/terraform/install-cli)
- [JQ](https://stedolan.github.io/jq/download/)

##### GCP Project
1. Take note of your Google Cloud Project ID. Moving forward, this will be referred to as `GCP_PROJECT_ID`.

##### Service Account
1. [Create a service account & download key JSON](https://cloud.google.com/docs/authentication/getting-started#creating_a_service_account). Moving forward, this will be referred to as `GCP_SERVICE_ACCOUNT`
1. Save the json file to `$HOME/.gcp` directory.
1. Grant Service Account the required IAM Policies.
   1. Modify [iam_policy.json](./service_account/iam_policy.json) & ensure the `serviceAccount` is yours.
   1. Set the policies.
      ```
      bash ./prereq/iam_policy_binding.sh -p <GCP_PROJECT_ID> -a <GCP_SERVICE_ACCOUNT>`
      ```
      ex:
      ```
      bash ./prereq/iam_policy_binding.sh -p jsandlin-c9fe7132 -a jsandlin-admin`
      ```
     - TODO: Break this down to permissions in a custom role (to increase security)
     - TODO: Currently this is granting more privs than required. Trim this down to bare minimum.
     - [REF](https://cloud.google.com/iam/docs/understanding-roles#predefined_roles)
  1. Enable required services.

     `bash ./prereq/services_enable.sh -p <GCP_PROJECT_ID>`

     ex:

     `bash ./prereq/services_enable.sh -p jsandlin-c9fe7132`
---
### GitLab

##### Create Agent Management projects
1. Create Cluster Management project
  - ex: [agentk-services](https://gitlab.com/sandlin/agentk-services)

  - Modify the `KUBE_CONTEXT`

1. Create Agent Registration project
  - ex: [agentk-registration](sandlin/agentk-registration)
  1. Modify the [config.yml](https://gitlab.com/sandlin/agentk-registration/-/blob/groot/.gitlab/agents/gitlab-agent/config.yaml)





##### GitLab Access Token
1. Create a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
   - Under `Select scopes`, select `api` and `read_registry`  
   - Save this token in your password management tool. It will not be accessable after this page is closed.
1. Create a local file `~/.gitlab/.creds` using this `Access Token`.
    ```
    # GitLab API token
    username="jsandlin"
    password="glpat-QerShxqUsPcAvfAp3xN3P"
    ```
